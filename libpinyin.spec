Name:           libpinyin
Version:        2.8.1
Release:        1
Summary:        Library to deal with pinyin
License:        GPLv3+
URL:            https://github.com/libpinyin/libpinyin
Source0:        http://downloads.sourceforge.net/libpinyin/libpinyin/%{name}-%{version}.tar.gz
BuildRequires:  gcc-c++ glib2-devel libdb-devel make
Provides:       libpinyin-data = %{version}-%{release}
Obsoletes:      libpinyin-data < %{version}-%{release}
Provides:       libpinyin-tools = %{version}-%{release}
Obsoletes:      libpinyin-tools < %{version}-%{release}
Provides:       libzhuyin = %{version}-%{release}
Obsoletes:      libzhuyin < %{version}-%{release}

%description
The libpinyin project aims to provide the algorithms core
for intelligent sentence-based Chinese pinyin input methods.


%package        devel
Summary:        Development files for libpinyin
Requires:       libpinyin = %{version}-%{release}
Provides:       libzhuyin-devel = %{version}-%{release}
Obsoletes:      libzhuyin-devel < %{version}-%{release}

%description    devel
This package includes header files and Development files for libpinyin

%package        help
Summary:        Document files for libpinyin

%description   help
Document files for libpinyin

%prep
%autosetup -p1

%build
%configure --disable-static --with-dbm=BerkeleyDB --enable-libzhuyin
%make_build

%check
make check

%install
%make_install
%delete_la

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%doc AUTHORS COPYING
%{_libdir}/libpinyin*.so.*
%dir %{_libdir}/libpinyin
%{_libdir}/libpinyin/data
%{_libdir}/libzhuyin*.so.*
%{_bindir}/gen_binary_files
%{_bindir}/import_interpolation
%{_bindir}/gen_unigram

%files devel
%dir %{_includedir}/libpinyin-%{version}
%{_includedir}/libpinyin-%{version}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc

%files help
%doc README
%{_mandir}/man1/*.1.*


%changelog
* Mon Sep 04 2023 chenchen <chen_aka_jan@163.com> - 2.8.1-1
- Upgrade to version 2.8.1

* Thu Jun 16 2022 SimpleUpdate Robot <tc@openeuler.org> - 2.6.2-1
- Upgrade to version 2.6.2

* Tue Feb 25 2020 zhujunhao <zhujunhao5@huawei.com> - 2.2.0-3
- Package init
